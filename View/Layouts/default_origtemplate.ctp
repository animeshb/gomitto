<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php //echo $this->Html->charset(); ?>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="<?php //echo $meta['keyWords']; ?>" />
    <meta name="description" content="<?php //echo $meta['metaDescription']; ?>" />
    <base href="<?php //echo $base_url; ?>"> -->
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">

	<?php
		//echo $this->Html->meta('icon');

		echo $this->Html->css('reset');
		echo $this->Html->css('jquery-ui-1.10.3.custom.min');
		echo $this->Html->css('layout');
	    echo $this->Html->script('jquery-1.8.3.min');
    
	    echo $this->Html->script('jquery-ui-1.10.3.custom.min.js');
        echo $this->Html->script('plugins');
        echo $this->Html->script('script');
 
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script>
    $(function() {
	    //$('#UserState').prev().hide();
	    //$('#UserState').hide();

         $('#fname').hide();
         $('#lname').hide();	    
	    
	    $('#UserMemberName').change( function() {

          var val = $(this).val();

          if(val==='individual')

             {

             	$('#compname').hide();
                $('#contact').hide();
                $('#fname').show();
                $('#lname').show();	
             }else {

             	$('#compname').show();
                $('#contact').show();
                $('#fname').hide();
                $('#lname').hide();
             }
          

	    });	
	    $('#UserCountry').change( function() {

    	//alert($(this).val());

        var val = $(this).val();
        if (val!='') {
            $('#othstate').val('').hide();
            $.ajax({
               type: 'POST',
               url: '<?php echo Router::url(array("controller"=>"states",
               	     "action"=>"ajaxcall"));?>',
               dataType: "json",
               data: { country : val },
               success: function(data) {
                   //alert(data[1]);
                   //$('#state').html( data );
                   var strhtml=''; 
                   for (var i = 0; i < data.length; i++) {
                   	  strhtml+='<option value="'+data[i]+'">'+data[i]+'</option>';
                   }
                   //$('#UserState').prev().show();
                   //$('#UserState').show();
                   $('#UserState').html(strhtml);
               }
            });
        }
        else {
           $('#state').val('').hide();
           $('#othstate').show();
        }
    });
});
	</script>
</head>
<body>
    <div id="wrapper">
        <div class="wrapper-container">
            <div id="header">
                <div class="header-container">
                    <div class="header-top-banner">
                        <div class="logo">
                    <a href="<?php echo Router::url(array("controller"=>"pages",
               	     "action"=>"display"));?>" target="_self">
               	     <img src="<?php echo $this->webroot;?>/images/logo.png" alt="Gomitto"></a>
                        </div>
                        <div class="login-frame">
                            <div class="without-login">
                                <div class="row">
                                    <a href="javascript:void(0);" class='menber-sign-up' rel="dialog-menber"><span>Menber Sign Up</span></a><br /><a href="javascript:void(0);" class="user-login" rel="dialog-user"><span>User Login</span></a>
                                </div>
                            </div>
                            <div class="logout"></div>
                        </div>
                    </div>
                     <div class="main-menu-region">
                        <div class="main-menu-body">
                            <div class="row">
                                <div class="main-menu">
                                <?php echo $this->element('menu'); ?>
                                </div>
                                <div class="top-menu">
                                 <?php echo $this->element('submenu'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="search">
                            <form action="">
                                <input type="text" value="Search" onblur="if (this.value=='') this.value='Search';" onfocus="if (this.value.toLowerCase()=='search') this.value='';" >
                                <input type="submit" value='Search' >
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="main">
               
<!-- Here's where I want my views to be displayed -->
<?php //echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>

<!--footer page-->
	    </div><!--end main body-->
	    <div id="footer">
            <div class="footer-container">
                <div class="row">
                    <div class="footer-menu">
                        <?php
                            //echo $footerMenuLayoutHTML;
                        ?>
                    </div>
                    <div class="social-media">
                        <span class="desc">Stay in Touch</span>
                        <ul>
                            <li><a target="_blank" href="" class="facebook"><span>Fackbook</span></a></li>
                            <li><a target="_blank" href="" class="twitter" ><span>Twitter</span></a></li>
                            <li><a target="_blank" href="" class="linkedin"><span>Linkedin</span></a></li>
                        </ul>
                    </div>
                    <div class="copyright">
                        <p>&copy; Copyright gomitto 2013. All Rights Reserved.<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Web design by <a href="">Design & PR Girl</a></p>
                    </div>
                </div>
            </div>
	    </div>
	    </div><!--end wrapper container-->
	</div><!--end wrapper-->
    
    <div class="dialog dialog-menber" title="Member Sign Up">
        <a href="<?php echo Router::url(array("controller"=>"users",
               	     "action"=>"add"));?>"><span>Go To Admin Console</span></a>
    </div>
    <div class="dialog dialog-user" title="User Login" >
    <a href="<?php echo Router::url(array("controller"=>"users",
               	     "action"=>"login"));?>">
        user Login
    </div>
</body>
</html>
