<?php 

// $lists = $this->requestAction('/members/memberlist');
// //debug($lists);
// $mem_types = array();
// foreach ($lists as $list) {
// 	$mid = $list['Member']['id'];
// 	$mname = $list['Member']['member_type'];
//     $mem_types[$mid]=$mname;
// }

$mem_types = array('individual'=>'individual','wholesale'=>'wholesale',
	               'retail'=>'retail','freight company'=>'freight company');

echo $this->Form->input(
    'member_name',
    array(
    'options' => $mem_types,
    'empty' => '(Please Select One)',
    'div' => false,
    'label'=>'Member Type:'
    )
);

 ?>