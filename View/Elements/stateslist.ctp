<?php

$lists = $this->requestAction('/states/stateslist/BD');
//debug($lists);
$mem_types = array();
foreach ($lists as $list) {
	$mname = $list['states']['state_name'];
    $mem_types[$mname]=$mname;
}

echo $this->Form->input(
    'state',
    array(
    'options' => $mem_types,
    'empty' => '(Please Select One)',
    'div' => false,
    'label'=>'State:'
    )
);
