<?php 

//this will take two input $role and $act_name
// echo $this->element('helpbox', array(
//     "helptext" => "Oh, this text is very helpful."
// ));


//action to label name ..... so that menu label name 
//can be selected from action name 
//index = controller_action

//$role = isset($role)?$role:'admin';


$act_label_name = array('users_edit'=>'Account Details',
                         'users_logout' => 'Logout',

                         'prices' => 'Freight prices',
                         'account_payable' => 'account payable',
                         'account' => 'account',
                         'admin' => 'admin',
                         'dispatch' => 'dispatch'
                         
	                    );

//common links for all user type
$common_opt=array(
        'users_edit' => 
          Router::url(array("controller"=>"users",
               	     "action"=>"edit",$ids)),
          'users_logout' => 
          Router::url(array("controller"=>"users",
                     "action"=>"logout")),
	);

//individual links based on role
$user_admin= array(
                 
              'admin' => '#'
	         );


$user_account= array(
                 'account' => '#'
	         );

$user_account_payable = array(
                  'account_payable'=>'#',
                  
	           );


$user_dispatch = array(
                  'dispatch'=>'#'
	           );



$side_bar = array(
               'admin' => array_merge($common_opt,$user_admin),
               'account' => array_merge($common_opt,$user_account),
               'account payable' => array_merge($common_opt,$user_account_payable),
               'dispatch' => array_merge($common_opt,$user_dispatch)
	         );

 ?>
<div class="aside-left">
    <h3><?php echo $role ?> Console</h3>
	    <div class="aside-menu">
		    <ul>
 <?php foreach ($side_bar[$role] as $key => $value): ?>
 	    <?php if ($key==$act_name): ?>
 	     	<li class=" active">
 	     <?php else: ?>
 	     	<li>
 	     <?php endif ?> 
	 			<a href="<?php echo $value ?>" target="_self" >
	 			    <span><?php echo $act_label_name[$key] ?></span>
	 			</a>
 			</li>
 <?php endforeach ?>
        	</ul>			
			</div>	
		</div>	
		