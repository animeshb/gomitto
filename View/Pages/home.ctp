<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div class="banner">
	<div class="banner-container">
		<div class="aside-bar">
			<div class="aside-menu">
				<div class="menu first-menu">
					<ul>
						<li><a href=""><span>Send a Parcel</span></a></li>
						<li><a href=""><span>Track a Parcel</span></a></li>
					</ul>
				</div>
				<div class="aside-search">
					<div class="search-iframe">
						<form action="">
							<input type="text" value="Search"  onblur="if (this.value=='') this.value='Search';" onfocus="if (this.value.toLowerCase()=='search') this.value='';" >
							<input type="submit">
							<a href=""><span>or find multiple items</span></a>
						</form>				
					</div>
				</div>
				<div class="menu">
					<ul>
						<li><a href=""><span>Reply Paid</span></a></li>
						<li><a href=""><span>Buy Mileage</span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="slideshow">
			<div class="slideshow-container">
				<div class="slide-item">
					<div class="img-container"><img src="images/img-slideshow-01.jpg" alt=""></div>
					<div class="desc">Send your freight at the best rate!</div>
				</div>
				<div class="slide-item">
					<div class="img-container"><img src="images/img-slideshow-02.jpg" alt=""></div>
					<div class="desc"> test Slideshow 2 </div>
				</div>			
			</div>
			<div class="slide-action">
				<ul></ul>
			</div>
		</div>
	</div>
</div>

<div class="mid-banner">
	<div class="mid-banner-container">
		<div class="row">
			<div class="bt-group">
				<div class="bt-group-bottom">
					<div class="bt-group-body">
						<div class="bt-container"><a href=""><span>GET A QUICK QUOTE</span></a></div>
						<div class="bt-container"><a href=""><span>BECOME A MEMBER</span></a></div>
						<div class="bt-container"><a href=""><span>MEMBER BENEFITS</span></a></div>
					</div>
				</div>
				
			</div>
			<div class="mid-banner-content">
				<div class="content-left">
				<h3>About gomitto</h3>
					<p>Here at Gomitto we allow our members to log in and use this website to book all
				types of freight.  It is a one stop shop for all your freight requirements.</p>
					<p>We source out the cheapest freight cost and pass them onto our club freight
				members regardless of parcel size.</p>
					<p>If you also refer a friend you’ll receive a $10 credit added to your account.</p>
					<div class="action-banner"><a href="" class="readmore button"><span>Read More</span></a></div>
				</div>
				<div class="content-right">
					<div class="adv">
						<span class="size-18">Refer a Friend<br>& a Friend</span>
						<span class="size-32">$10 Credit</span>
						<span class="size-14">added to <br>your account</span>
						<a href="" class="readmore"><span>Detail</span></a>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>

<div class="blocks-collection">
	<div class="row">
		<div class="block-item newsletter">
			<h3>Join Our Newsletter</h3>
			<div class="content">
				<p>Subscribe to find out latest news, special promotions and offers...</p>
				<form action="">
					<p></p>
					<input type="text">
					<input type="text">
					<div class="action"><input type="submit" value="Subscribe"></div>				
				</form>
			</div>
		</div>
		<div class="block-item our-partners">
			<h3>Our Partners</h3>
			<div class="content">
				<ul>
					<li><a href=""><img src="images/icon-partners-dhl.png" alt="dhl"></a></li>
					<li><a href=""><img src="images/icon-partners-toll.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-dhl.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-toll.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-hx.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-direct.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-hx.png" alt=""></a></li>
					<li><a href=""><img src="images/icon-partners-direct.png" alt=""></a></li>
				</ul>
			</div>
		</div>
		<div class="block-item video">
			<h3>What our Members are Saying</h3>
			<div class="content"><iframe width="286" height="161" src="//www.youtube.com/embed/uOTWy3CNvNA" frameborder="0" allowfullscreen></iframe></div>
		</div>
		<div class="clearboth"></div>
	</div>
</div>