<div class="businesses view">
<h2><?php echo __('Business'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($business['Business']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Member Id'); ?></dt>
		<dd>
			<?php echo h($business['Business']['member_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Business Name'); ?></dt>
		<dd>
			<?php echo h($business['Business']['business_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Business Type'); ?></dt>
		<dd>
			<?php echo h($business['Business']['business_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address Id'); ?></dt>
		<dd>
			<?php echo h($business['Business']['address_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Preferred Carrier'); ?></dt>
		<dd>
			<?php echo h($business['Business']['preferred_carrier']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Abn'); ?></dt>
		<dd>
			<?php echo h($business['Business']['abn']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Only Lowest'); ?></dt>
		<dd>
			<?php echo h($business['Business']['only_lowest']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($business['Business']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($business['Business']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Business'), array('action' => 'edit', $business['Business']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Business'), array('action' => 'delete', $business['Business']['id']), null, __('Are you sure you want to delete # %s?', $business['Business']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Businesses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business'), array('action' => 'add')); ?> </li>
	</ul>
</div>
