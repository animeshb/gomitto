<div class="businesses form">
<?php echo $this->Form->create('Business'); ?>
	<fieldset>
		<legend><?php echo __('Add Business'); ?></legend>
	<?php
		echo $this->Form->input('member_id');
		echo $this->Form->input('business_name');
		echo $this->Form->input('business_type');
		echo $this->Form->input('address_id');
		echo $this->Form->input('preferred_carrier');
		echo $this->Form->input('abn');
		echo $this->Form->input('only_lowest');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Businesses'), array('action' => 'index')); ?></li>
	</ul>
</div>
