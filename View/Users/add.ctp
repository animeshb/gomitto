            <?php $this->extend('/Common/usercommon'); ?>
            <?php //if ($authUser)pr('defined');die(); ?>
            <?php $act_name = $this->request->params['action'];?>
            <div class="account-details">
				<h3>Account Details</h3>
                <h4><?php echo $this->Session->flash(); ?></h4>  
                <?php echo $this->Form->create('User',array('div' => false,
                           'class'=>'common-form')); ?>
					<div class="form-body">
						<ul>
						   <li id='fname'>
							<?php echo $this->Form->input('first_name',
							      array('div' => false,'label'=>'First Name:')); ?>
							</li>
							<li id='compname'>
							<?php echo $this->Form->input('company_name',
							      array('div' => false,'label'=>'Company:')); ?>
							</li>
							<li>
                             <?php echo $this->Form->input('username',array('div' => false,'label'=>'Username/Email:')); ?>
							</li>
							<li id='lname'>
							<?php echo $this->Form->input('last_name',
							      array('div' => false,'label'=>'Last Name:')); ?>
							</li>
							<li  id='contact'>
							<?php echo $this->Form->input('name',
							      array('div' => false,'label'=>'Contact:')); ?>
							</li>
							<?php if ($act_name=='add'): ?>
							<li>
							<?php echo $this->Form->input('password',
							      array('div' => false,'label'=>'Password:',
							      'required'=>'required')); ?>
							</li>	
							<?php endif ?>
							
							<li id='abn'>
							<?php echo $this->Form->input('abn',
							      array('div' => false,'label'=>'ABN:')); ?>
                            </li>
                            <?php if ($act_name=='add'): ?>
                            	<li>
							<?php echo $this->Form->input('confpassword',
							      array('type'=>'password','div' => false,
							      'label'=>'ConfirmPassword:','required'=>'required')); ?>
							</li>
                            <?php endif ?>
							
						</ul>
						<ul>
						<?php if ($act_name=='add'): ?>
					     <li><?php echo $this->element('memberlist'); ?></li>
						<?php endif ?>
						
						
						<li>
                            <?php echo $this->Form->input('address1',
							      array('div' => false,'label'=>'Billing Address:')); ?>
							</li>
							<li>
                            <?php echo $this->Form->input('city',
							      array('div' => false,'label'=>'Suburb:')); ?>  
							</li>
							<li>
							<?php echo $this->element('countrylist');?>
							</li>
							<li>
                            <?php echo $this->element('stateslist'); ?> 
							</li>
							<li>&nbsp</li>
							<li><?php echo $this->Form->input('postcode',
							      array('div' => false,'label'=>'Postcode:')); ?>  
							</li>
						</ul>
					</div>
					<div class="from-action">
					    <input name="data[User][role]" value="admin" id="UserRole" type="hidden" />
						<input type="submit" value="save">

					</div>
				</form>
			</div>
            