<div class="inner-page-body">
	<?php echo $this->element('breadbumb'); ?>
	<div class="admin-console-frame">
     <?php //pr($this->Auth); ?>
     <?php  
        echo $this->element('user_menu', array(
		    "act_name" =>$this->params['controller'].'_'.$this->params['action'] 
		));
     ?>
<div class="aside-right">
			<div class="account-details">
				<h3>Account Details</h3>
                <h4><?php echo $this->Session->flash(); ?></h4>  
                <?php echo $this->Form->create('User',array('div' => false,
                           'class'=>'common-form')); ?>
					<div class="form-body">
						<ul>
							<!-- <li id='fname'>
							<?php //echo $this->Form->input('first_name',
							      //array('div' => false,'label'=>'First Name:')); ?>
							</li>
							 -->
						
							<li id='compname'>
							<?php echo $this->Form->input('company_name',
							      array('div' => false,'label'=>'Company:')); ?>
							</li>
							<li>
                             <?php echo $this->Form->input('username',array('div' => false,'label'=>'Username/Email:')); ?>
							</li>
							<li  id='contact'>
							<?php echo $this->Form->input('name',
							      array('div' => false,'label'=>'Contact:')); ?>
							</li>
							<!-- <li id='lname'>
                             <?php //echo $this->Form->input('last_name',
                                   //array('div' =>  false,'label'=>'Last Name:')); ?>
							</li> -->
							<!-- <li>
							<?php //echo $this->Form->input('password',
							      //array('div' => false,'label'=>'Password:',
							      //'required'=>'required')); ?>
							</li> -->
							<li>
							<?php echo $this->Form->input('abn',
							      array('div' => false,'label'=>'ABN:')); ?>
                            </li>
							<li>
							<?php //echo $this->Form->input('confpassword',
							//       array('type'=>'password','div' => false,
							//       'label'=>'ConfirmPassword:','required'=>'required')); ?>
							</li>
						</ul>
						<ul>
						<li><?php //echo $this->element('memberlist'); ?>
                            <?php //echo $this->Form->input('member_id',
							      //array('div' => false,'label'=>'Member Type:')); ?>
						</li>
							<li>
                            <?php echo $this->Form->input('address1',
							      array('div' => false,'label'=>'Billing Address:')); ?>
							</li>
							<li>
                            <?php echo $this->Form->input('city',
							      array('div' => false,'label'=>'Suburb:')); ?>  
							</li>
							<li>
							<?php //echo $this->Form->input('country',
							      //array('div' => false,'label'=>'Country:')); 
                             //pr($this->request->data['User']['country']);die();
							?>
                            
							<?php echo $this->element('countrylist',
							                          array("selcted" =>$this->request->data['User']['country']))?>
							</li>
							<li>
							<?php //echo $this->Form->input('state',
							      //array('div' => false,'label'=>'State:')); ?>
                            <?php echo $this->element('stateslist'); ?> 
							</li>
							<li>&nbsp</li>
							<li><?php echo $this->Form->input('postcode',
							      array('div' => false,'label'=>'Postcode:')); ?>  
							</li>
						</ul>
					</div>
					<div class="from-action">
					    <input name="data[User][role]" value="admin" id="UserRole" type="hidden" />
						<input type="submit" value="save">

					</div>
				</form>
			</div>
		</div>
		<div class="clearboth"></div>
	</div>
	<div class="clearboth"></div>
</div>
	