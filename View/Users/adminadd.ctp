<?php $this->extend('/Common/usercommon'); ?>

<div class="sv-form">
        	<h3>Enter User Details</h3>
                <h4><?php echo $this->Session->flash(); ?></h4>  
                <?php echo $this->Form->create('User',array('div' => false,
                           'class'=>'common-form')); ?>
				<div class="form-body">
					<ul>
							<li>
							<?php echo $this->Form->input('first_name',
							      array('div' => false,'label'=>'First Name:')); ?>
							</li>
							<li>
							<?php echo $this->Form->input('last_name',
							      array('div' => false,'label'=>'Last Name:')); ?>
							</li>
                            <li>
                            <label for="UserRole">Role:</label>
							<?php echo $this->element('rolelist');?>
                            </li>
							<li>
                             <?php echo $this->Form->input('username',array('div' => false,'label'=>'Email:')); ?>
							</li>
							
							<li>
							<?php echo $this->Form->input('password',
							      array('div' => false,'label'=>'Password:',
							      'required'=>'required')); ?>
							</li>	
						
                            <li>
							<?php echo $this->Form->input('confpassword',
							      array('type'=>'password','div' => false,
							      'label'=>'ConfirmPassword:','required'=>'required')); ?>
							</li>
							
						</ul>
				</div>
				<div class="from-action">
					<input type="submit" value="save">
				</div>
			</form>
        </div>

