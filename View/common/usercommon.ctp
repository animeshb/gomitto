
<div class="inner-page-body">
	<?php echo $this->element('breadbumb'); ?>
	<div class="admin-console-frame">
	<?php $authUser = isset($authUser)?$authUser:false; ?>
     <?php if ($authUser): ?>
     <?php  
        echo $this->element('user_menu', array(
		    "act_name" =>$this->params['controller'].'_'.$this->params['action'] 
		));
     ?>
     <div class="aside-right">
     <?php endif ?>
	<?php echo $this->fetch('content'); ?>
<?php if ($authUser): ?>
		</div>
<?php endif ?>		
		<div class="clearboth"></div>
	</div>
	<div class="clearboth"></div>
</div>
	