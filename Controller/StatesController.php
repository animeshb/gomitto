<?php
App::uses('AppController', 'Controller');
/**
 * States Controller
 *
 * @property State $State
 * @property PaginatorComponent $Paginator
 */
class StatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


	public function beforeFilter() {
	  parent::beforeFilter();
	// Allow users to register and logout.
	  $this->Auth->allow('stateslist','ajaxcall','services_getstates');
	
	}


public function services_getstates() {

            if ($this->request->is('ajax')) {
              $data = $this->request->data['country'];
             // //echo $data;


             $result = $this->stateslist($data);
             $results = array();
             foreach ($result as $value) {
             	$results[]=$value['states']['state_name'];
             }

             echo json_encode($results);exit();

           }


}	

	public function ajaxcall() {
  //           //echo "animesh";
		//      //pr($this->request->data['country']);
		//        $data = $this->request->data['country'];
  //      //       //echo $data;
  //      //       pr($this->stateslist($data));
		//      // //$this->autoRender=false;
        
		// $result = $this->stateslist($data);
		// pr($result);
  //            $results = array();
  //            foreach ($result as $value) {
  //            	$results[]=$value['states']['state_name'];
  //            }

  //            pr($results);

            if ($this->request->is('ajax')) {
              $data = $this->request->data['country'];
             // //echo $data;


             $result = $this->stateslist($data);
             $results = array();
             foreach ($result as $value) {
             	$results[]=$value['states']['state_name'];
             }

             echo json_encode($results);exit();

           }

		}


	public function stateslist($name='AUS') {

           $query = "SELECT state_name FROM states where country_code ='".$name."';";

			return $this->State->query($query);
			//$this->State->find('all');
			
		}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->State->recursive = 0;
		$this->set('states', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->State->exists($id)) {
			throw new NotFoundException(__('Invalid state'));
		}
		$options = array('conditions' => array('State.' . $this->State->primaryKey => $id));
		$this->set('state', $this->State->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->State->create();
			if ($this->State->save($this->request->data)) {
				$this->Session->setFlash(__('The state has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The state could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->State->exists($id)) {
			throw new NotFoundException(__('Invalid state'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->State->save($this->request->data)) {
				$this->Session->setFlash(__('The state has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The state could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('State.' . $this->State->primaryKey => $id));
			$this->request->data = $this->State->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->State->id = $id;
		if (!$this->State->exists()) {
			throw new NotFoundException(__('Invalid state'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->State->delete()) {
			$this->Session->setFlash(__('The state has been deleted.'));
		} else {
			$this->Session->setFlash(__('The state could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
