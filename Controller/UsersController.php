<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	

	public function beforeFilter() {
	parent::beforeFilter();
	// Allow users to register and logout.
	$this->Auth->allow('autologin','add', 'logout','index','services_test','userview','services_register');

	}


	

	function update_last_login_time($user_id){

        $query = "UPDATE users SET last_logged_in= NOW() WHERE id=".$user_id;
		// echo $query;
        //debug($this->Auth->login);
		 //die();
	    $this->User->query($query);
	}

	public function login() {
    if ($this->request->is('post')) {

        if ($this->Auth->login()) {
        	// pr($this->request->data);die();
            $this->update_last_login_time($this->Auth->user('id'));
            return $this->redirect($this->Auth->redirect());
        }
        $this->Session->setFlash(__('Invalid username or password, try again'));
    }
}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}


public function userview() {
	   
      $this->set_menu_option();
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;

        // test basis should be added in the logged 
		//$this->set('role', $this->Auth->user())
         //pr($this->Auth->user());

		$this->set('users', $this->Paginator->paginate());
		//debug($all_members);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */

public function services_test() {

	// echo "You have successfully registered";
	

	// echo Router::getParam('prefix', true);
	// exit;
   $data = array('Status'=>'success','message'=>'test msg', 'data' => $this->User->find('first'));
   $this->set('data',$data);
}

public function services_logout() {

    
       $this->Auth->logout();
       $data = array('Status'=>'success','message'=>'Logout Successfully', 'data' => array());
       $this->set('data',$data);


}

public function services_register() {

	// echo "You have successfully registered";
	

	// echo Router::getParam('prefix', true);
	// exit;

	

	if ($this->User->save($this->setdata())){
			//if ($this->User->save($user_data)) {
			$this->update_last_login_time($this->User->id);
			$user = $this->User->findById($this->User->id);
			$user = $user['User'];
			$this->Auth->login($user);


			$data = array('Status'=>'success','message'=>'Registration done', 'data' => array('id'=>$this->User->id,'username'=>$this->User->read('username')));
            $this->set('data',$data);
			} else {
				$data = array('Status'=>'fail','message'=>'Not done', 'data' => array('Not done yet'));
                $this->set('data',$data);
			}


   //exit();
}


public function autologin(){

    
   $this->set('data',$this->Auth->User());

}
    
	public function add() {
       
			

		if($this->Auth->login()){
			$this->set_menu_option();
			$this->render('adminadd');
		}

		if ($this->request->is('post')) {
		
            // debug($this->request);
            // die();confpassword
            if($this->request->data['User']['confpassword']!=$this->request->data['User']['password']) {
            	$this->Session->setFlash(__('password and confirm password field mismatch!'));
            	return;
            }

        	$this->User->create();


		if($this->Auth->user()){
              
            $this->request->data['User']['member_id']=$this->Auth->user('member_id');
            //debug($this->request->data);die();
            if ($this->User->save($this->request->data)) {
            	//debug($this->User);die();
            	$this->update_last_login_time($this->User->id);
				$this->Session->setFlash(__('The user has been saved.'));
				$this->set_menu_option();
				return $this->redirect($this->Auth->redirect());
			}
			else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again11.'));
			    return;
			}	

		}

		    if ($this->User->save($this->setdata())){
			//if ($this->User->save($user_data)) {
				$this->update_last_login_time($this->User->id);
				$this->Session->setFlash(__('The user has been saved.'));
				$this->set_menu_option();
				//pr($this->Auth);die();
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} 
	}

// this will set 3 table address,member,business
//used in add&&edit	

function setdata(){

       //pr($this->Auth->User());die();

		$this->loadModel('Business');
		$this->loadModel('Address');
        $this->loadModel('Member');
 			
         $all_data = $this->request->data['User'];

        

         $mem_data = array(
				                'Member' => array(
		                                       
		   									    'member_type' => $all_data['member_name'],
		   									    
										        )
				          );
         $this->Member->save($mem_data);
         $memid = $this->Member->id;
               
         //extract($all_data);
         $fname = isset($all_data['first_name'])?$all_data['first_name']:'';
         $lname = isset($all_data['last_name'])?$all_data['last_name']:'';
         $company_name = isset($all_data['company_name'])?$all_data['company_name']:'';
         $name = isset($all_data['name'])?$all_data['name']:'';
         $abn = isset($all_data['abn'])?$all_data['abn']:'';
          
        
			$user_data = array(
				                'User' => array(
		                                        'password' => $all_data['password'],
		  										'first_name' => $fname,
												'last_name' => $lname,
		   							  		    'username' => $all_data['username'],
		   									    'member_id' => $memid,
		   									    'role' => $all_data['role']
		   									    
										        )
				          );

                $address_data = array(
				                'Address' => array(
		                                       'city' => $all_data['city'],
		  										'state' => $all_data['state'],
												'country' => $all_data['country'],
		   									    'postcode' => $all_data['postcode'],
		   									    'member_id' => $memid,
		   									    'name' => $name,
		   									    'company_name' => $company_name
										        )
				          );
                
                $this->Address->save($address_data);
                $addressid = $this->Address->id;
                

                $business_data = array(
				                'Business' => array(
		                                        'abn' => $abn,
		   									    'member_id' => $memid,
		   									    'address_id' => $addressid,
		   									    'business_name' => $company_name
										        )
				          );


                $this->Business->save($business_data);
			

    
         return $user_data;


}


function set_menu_option(){

// set user menu based on the role and sidebar for the logged in users

	if($this->Auth->user()){
        $this->set('role', $this->Auth->user('role'));
        $this->set('ids', $this->Auth->user('id'));
        $this->set('authUser', $this->Auth->user());
    }else {

    	$this->set('role', $this->request->data['User']['role']);
        $this->set('ids', $this->User->id);
        $this->set('authUser', true);
    }
}	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

            $this->set_menu_option();

            $this->loadModel('Business');
			$this->loadModel('Address');

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {

			//$this->Auth->User('member_id'),$id

	        $post_val = $this->request->data['User'];
            extract($post_val);


				$this->Business->read(array('abn'),
					                  $this->Auth->User('member_id'));
				$this->Business->set(array(
				    
				    
				    'abn' => $abn,

				));
				$this->Business->save();

                $this->Address->read(array('company_name','name','city','state','country','postcode'), 
                	                $this->Auth->User('member_id'));
				$this->Address->set(array(
				    'city' => $city,
				    'state' => $state,
				    'country' => $country,
				    'postcode' => $postcode,
				    'company_name' => $company_name,
				    'name' =>$name

				));
				$this->Address->save();

               $this->User->read(array('username'), $id);
				$this->User->set(array(
				    'username' => $username
				));
				$this->User->save();

				//pr($this->User->save());die();

   //           if ($this->User->save($this->setdata())){
			// //if ($this->User->save($this->request->data)) {
			// 	$this->Session->setFlash(__('The user has been saved.'));
			// 	return $this->redirect(array('action' => 'index'));
			// } else {
			// 	$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			// }
		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
  
			$options = array('conditions' => array('Address.member_id' => $this->Auth->User('member_id')));
			$address_query = $this->Address->find('first', $options);
            $address_query = $address_query['Address'];
            extract($address_query);

            $options = array('conditions' => array('Business.member_id' => $this->Auth->User('member_id')));
			$business_query = $this->Business->find('first', $options);
            $business_query = $business_query['Business'];
            extract($business_query);


			// $this->request->data['User']['city'] = $address_query['Address']['city'];
   //          $this->request->data['User']['state'] = $address_query['Address']['state'];
   //          $this->request->data['User']['country'] = $address_query['Address']['country'];
   //          $this->request->data['User']['postcode'] = $address_query['Address']['postcode'];
			
			//pr($this->request->data);die();

            $this->request->data['User']['city'] = $city;
            $this->request->data['User']['state'] = $state;
            $this->request->data['User']['country'] = $country;
            $this->request->data['User']['postcode'] = $postcode;
            $this->request->data['User']['company_name'] = $company_name;
            $this->request->data['User']['name'] = $name;
            $this->request->data['User']['abn'] = $abn;
            $this->request->data['User']['address1'] = $address1;

            //pr($address_query);die();


            $this->render('add');

		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
